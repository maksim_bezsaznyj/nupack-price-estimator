#Rule which always applies a specified flat markup
module NupackPriceEstimator
  class FlatMarkupRule
    def initialize(flat_percentage)
      @percentage = flat_percentage
    end

    def process(context)
      context[:flat_markup] = calc_new_flat_markup(context)
    end

    def calc_new_flat_markup(context)
      context.fetch(:flat_markup, 0) + @percentage
    end
  end
end