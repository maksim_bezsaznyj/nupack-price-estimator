require 'rules/final_price_rule'

describe 'NupackPriceEstimator::FinalPriceRule' do
  subject(:rule) { NupackPriceEstimator::FinalPriceRule.new }

  describe '#validate_context' do
    it 'should fail if there is no base price' do
      expect { rule.validate_context({}) }
          .to raise_error(NupackPriceEstimator::NoBasePriceError)
    end

    it 'should not fail if there is a base price' do
      expect { rule.validate_context(base_price: 123.0) }
          .to_not raise_error
    end
  end

  describe '#calc_final_price' do
    it 'should make base price the final price on no markups' do
      expect(rule.calc_final_price(
          base_price: 132.5
      )).to eq(132.5)
    end

    it 'should apply all markups summed' do
      expect(rule.calc_final_price(
          base_price: 132.5,
          flat_markup: 6.5,
          additional_markup: 6.51
      )).to eq(150.30)
    end

    it 'should round down by half-up rule' do
      expect(rule.calc_final_price(
          base_price: 132.444
      )).to eq(132.44)
    end

    it 'should round up by half-up rule' do
      expect(rule.calc_final_price(
          base_price: 132.445
      )).to eq(132.45)
    end
  end

  describe '#process' do
    it 'should validate input params' do
      ctx = {}
      expect(rule).to receive(:validate_context).with(ctx)
      allow(rule).to receive(:calc_final_price).with(ctx).and_return 123.45
      rule.process(ctx)
    end

    it 'should put calculation result into context' do
      ctx = {}
      allow(rule).to receive(:validate_context).with(ctx)
      expect(rule).to receive(:calc_final_price).with(ctx).and_return 123.45
      expect { rule.process(ctx) }.to change { ctx[:final_price] }.to 123.45
    end
  end
end
