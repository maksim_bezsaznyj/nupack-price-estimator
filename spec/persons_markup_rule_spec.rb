require 'rules/persons_markup_rule'

describe 'NupackPriceEstimator::PersonsMarkupRule' do
  subject(:rule) { NupackPriceEstimator::PersonsMarkupRule.new(2.0) }

  describe '#calc_person_markup' do
    it 'multiplies person count by constant percentage' do
      expect(rule.calc_person_markup(5)).to eq(10.0)
    end
  end

  describe '#calc_final_markup' do
    it 'adds calculated markup to markup in context' do
      expect(rule).to receive(:calc_person_markup).with(5).and_return 10
      expect(rule.calc_final_markup(
          additional_markup: 1.0,
          persons_needed: 5
      )).to eq(11.0)
    end

    it 'handles no starting additional markup' do
      expect(rule).to receive(:calc_person_markup).with(5).and_return 10
      expect(rule.calc_final_markup(persons_needed: 5)).to eq(10.0)
    end

    it 'adds no markup if no persons are needed' do
      expect(rule).to receive(:calc_person_markup).with(0).and_return 0
      expect(rule.calc_final_markup(persons_needed: 0)).to eq(0.0)
    end

    it 'handles no persons needed fact in context' do
      expect(rule).to receive(:calc_person_markup).with(0).and_return 0
      expect(rule.calc_final_markup({})).to eq(0.0)
    end
  end

  describe '#process' do
    it 'should put new calculated markup into context' do
      ctx = {}
      expect(rule).to receive(:calc_final_markup).with(ctx).and_return 15.55
      expect { rule.process(ctx) }
          .to change { ctx[:additional_markup] }.to 15.55
    end
  end
end