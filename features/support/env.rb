Transform /^(-?[\d.]+)$/ do |number|
  number.to_f
end

Transform /^(-?\d+)$/ do |number|
  number.to_i
end