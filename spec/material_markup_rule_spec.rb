require 'rules/material_markup_rule'

describe 'NupackPriceEstimator::MaterialMarkupRule' do
  subject(:rule) { NupackPriceEstimator::MaterialMarkupRule.new(%w(food apples), 13.0) }

  describe '#process' do
    it 'should put new calculated markup into context if rule applies' do
      ctx = {}
      allow(rule).to receive(:markup_applies?).with(ctx).and_return true
      expect(rule).to receive(:calc_final_markup).with(ctx).and_return 15.55
      expect { rule.process(ctx) }
          .to change { ctx[:additional_markup] }.to 15.55
    end

    it 'should do nothing if rule does not apply' do
      ctx = {}
      allow(rule).to receive(:markup_applies?).with(ctx).and_return false
      expect(rule).to_not receive(:calc_final_markup)
      rule.process(ctx)
    end
  end

  describe '#markup_applies?' do
    it 'should apply if any material matches the rule' do
      expect(rule.markup_applies?(contains_material: ['food'])).to eq(true)
    end

    it 'should not apply if no material matches the rule' do
      expect(rule.markup_applies?(contains_material: ['other'])).to eq(false)
    end

    it 'can handle missing fact in context' do
      expect(rule.markup_applies?({})).to eq(false)
    end
  end

  describe '#calc_final_markup' do
    it 'should apply constant markup for related material products' do
      expect(rule.calc_final_markup(
          contains_material: %w(food apples stuff)
      )).to eq(13.0)
    end

    it 'should add to existing additional markup' do
      expect(rule.calc_final_markup(
          contains_material: %w(food apples stuff),
          additional_markup: 1.0
      )).to eq(14.0)
    end
  end
end