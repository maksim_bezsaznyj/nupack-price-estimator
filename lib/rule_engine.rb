module NupackPriceEstimator
  class RuleEngine
    def initialize(*rules)
      @rules = rules
    end

    def process(facts)
      @rules.each { |r| r.process(facts) }
    end
  end
end