Gem::Specification.new do |s|
  s.name        = 'nupack_price_estimator'
  s.version     = '1.0.0'
  s.licenses    = ['MIT']
  s.summary     = 'NuPack price estimation module'
  s.description = 'A Ruby library designed to estimate contract packaging prices depending on a varying set of business rules.'
  s.authors     = ['Maksim Bezsaznyj']
  s.email       = 'bezmax@gmail.com'
  s.files       = Dir['lib/**/*.rb']
  s.homepage    = 'https://bitbucket.org/maksim_bezsaznyj/nupack-price-estimator'
end