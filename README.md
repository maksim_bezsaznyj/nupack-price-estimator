#NuPack price estimation module
##Summary
A Ruby library designed to estimate contract packaging prices depending 
on a varying set of business rules.

##Prerequisites
* Ruby version 2.3

##Preparing for use
* Change the directory to the project root directory (the one containing this file).
* Execute `gem install bundler` command.
* Execute `bundler install` command.

##Running tests
* For acceptance tests run `cucumber` command. 
* For unit tests run `rspec` command.
* To check the test coverage use:


    bundle exec mutant --include lib  --use rspec NupackPriceEstimator*

##Implementation
Due to the fact that "price estimation" is a very business-logic-heavy 
domain, the module uses a simple business-rule engine to process all
the rules. The business-rule engine applies all the registered rules
one by one to the set of facts in the knowledge-base, resulting in new
facts.

This approach allows straightforward addition of new rules without
cluttering the codebase. Also, it makes the code much easier to test,
debug and document, as all of the components are very loosely coupled.

##Usage
First of all, rule engine has to be instantiated with all the rules
that are to be applied:

    engine = RuleEngine.new(
                                 FlatMarkupRule.new(5.0),
                                 MaterialMarkupRule.new(['food'], 13.0),
                                 MaterialMarkupRule.new(['drugs'], 7.5),
                                 MaterialMarkupRule.new(['electronics'], 2.0),
                                 PersonsMarkupRule.new(1.2),
                                 FinalPriceRule.new
                             )
                             
Then, the knowledge-base has to be filled with known facts about the product: 
    
    facts = {
        base_price: 13.50,
        contains_material: ['food', 'electronics', 'unobtainium'],
        persons_needed: 6
    }
    
Lastly, pass the list of facts to the engine. Be warned, that even though the
engine and the rules themselves are stateless, the list of facts are mutable.

    engine.process(facts)
    
The calculation result and intermediate values can be seen in the fact context:

    facts[:final_price]
    facts[:flat_markup]
    facts[:additional_markup]
    
##Future considerations
Current implementation of the rule engine is very trivial and is not suitable
for complex facts/rules. In case the complexity of rules grows, a full-blown 
RETE algorithm implementation has to be applied.

Another, even better option, is to use a dedicated rule-engine implementation 
such as Drools, wongi, Ruleby or others.