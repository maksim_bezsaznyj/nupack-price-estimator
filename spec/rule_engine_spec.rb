require 'rule_engine'

describe 'NupackPriceEstimator::RuleEngine' do
  describe '#process' do
    it 'should apply all rules to facts' do
      facts = double

      rule1 = spy
      rule2 = spy

      allow(rule1).to receive(:process).with(facts).and_return(facts)
      allow(rule2).to receive(:process).with(facts).and_return(facts)

      NupackPriceEstimator::RuleEngine.new(rule1, rule2).process(facts)

      expect(rule1).to have_received(:process).once
      expect(rule2).to have_received(:process).once
    end
  end
end