require 'rules/no_base_price_error'
#Rule which calculates a final price from base price and markups
#This rule should be applied last
module NupackPriceEstimator
  class FinalPriceRule
    def process(context)
      validate_context(context)
      context[:final_price] = calc_final_price(context)
    end

    def validate_context(context)
      raise NoBasePriceError unless context.has_key?(:base_price)
    end

    def calc_final_price(context)
      base_price = context.fetch(:base_price)
      flat_markup = context.fetch(:flat_markup, 0)
      additional_markup = context.fetch(:additional_markup, 0)

      (base_price * (1.0 + flat_markup/100.0) * (1.0 + additional_markup/100.0)).round(2)
    end
  end
end