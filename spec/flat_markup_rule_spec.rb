require 'rules/flat_markup_rule'

describe 'NupackPriceEstimator::FlatMarkupRule' do
  subject(:rule) { NupackPriceEstimator::FlatMarkupRule.new(5.0) }

  describe '#process' do
    it 'should put new calculated markup into context' do
      ctx = {}
      allow(rule).to receive(:calc_new_flat_markup).with(ctx).and_return 123.45
      expect { rule.process(ctx) }
          .to change { ctx[:flat_markup] }.to 123.45
    end
  end

  describe '#calc_new_flat_markup' do
    it 'should add to existing flat markup' do
      expect(rule.process(flat_markup: 1.0)).to eq 6.0
    end

    it 'should use 0 as default markup' do
      expect(rule.process({})).to eq 5.0
    end
  end
end