Feature: Markup application
  In order to calculate estimated price
  As an external webservice
  I should be able to pass in facts about product and receive calculated prices

  Scenario: Simple product
    Given product contains no special materials
    And it's base price is $1299.99
    When I calculate it's final price
    Then Flat markup of 5% is applied
    And Additional markup is not applied
    And The final price is $1364.99

  Scenario: Food product
    Given product contains food
    And it's base price is $1299.99
    When I calculate it's final price
    Then Flat markup of 5% is applied
    And Additional markup of 13% is applied
    And The final price is $1542.44

  Scenario: Drug product
    Given product contains drugs
    And it's base price is $1299.99
    When I calculate it's final price
    Then Flat markup of 5% is applied
    And Additional markup of 7.5% is applied
    And The final price is $1467.36

  Scenario: Electronics product
    Given product contains electronics
    And it's base price is $1299.99
    When I calculate it's final price
    Then Flat markup of 5% is applied
    And Additional markup of 2% is applied
    And The final price is $1392.29

  Scenario: Simple product requiring 2 persons
    Given product contains no special materials
    And it's base price is $1299.99
    And it requires 2 persons
    When I calculate it's final price
    Then Flat markup of 5% is applied
    And Additional markup of 2.4% is applied
    And The final price is $1397.75

  Scenario: Food product requiring 3 persons
    Given product contains food
    And it's base price is $1299.99
    And it requires 3 persons
    When I calculate it's final price
    Then Flat markup of 5% is applied
    And Additional markup of 16.6% is applied
    And The final price is $1591.58

  Scenario: Drug product requiring 1 person
    Given product contains drugs
    And it's base price is $5432.00
    And it requires 1 person
    When I calculate it's final price
    Then Flat markup of 5% is applied
    And Additional markup of 8.7% is applied
    And The final price is $6199.81

  Scenario: Books product requiring 4 persons
    Given product contains books
    And it's base price is $12456.95
    And it requires 4 persons
    When I calculate it's final price
    Then Flat markup of 5% is applied
    And Additional markup of 4.8% is applied
    And The final price is $13707.63

  Scenario: Complex product requiring 10 persons
    Given product contains books, food, drugs, electronics, wood
    And it's base price is $100000.00
    And it requires 10 persons
    When I calculate it's final price
    Then Flat markup of 5% is applied
    And Additional markup of 34.5% is applied
    And The final price is $141225.00