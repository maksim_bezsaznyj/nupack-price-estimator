#Configurable rule which applies additional markup if specific type of
#material is found in product
module NupackPriceEstimator
  class MaterialMarkupRule
    def initialize(types, percentage)
      @types = types
      @percentage = percentage
    end

    def process(context)
      if markup_applies?(context)
        context[:additional_markup] = calc_final_markup(context)
      end
    end

    def calc_final_markup(context)
      context.fetch(:additional_markup, 0) + @percentage
    end

    def markup_applies?(context)
      (context.fetch(:contains_material, []) & @types).any?
    end
  end
end