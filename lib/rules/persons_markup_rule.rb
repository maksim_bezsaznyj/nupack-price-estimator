#For each person that needs to work on the job, there is a flat markup applied
#The per-person markup is specified through constructor
module NupackPriceEstimator
  class PersonsMarkupRule
    def initialize(markup_per_person)
      @markup_per_person = markup_per_person
    end

    def process(context)
      context[:additional_markup] = calc_final_markup(context)
    end

    def calc_person_markup(persons_needed)
      @markup_per_person * persons_needed
    end

    def calc_final_markup(context)
      added_markup = calc_person_markup(context.fetch(:persons_needed, 0))
      context.fetch(:additional_markup, 0) + added_markup
    end
  end
end