require 'nupack_price_estimator'

include NupackPriceEstimator

Before do
  @engine = RuleEngine.new(
      FlatMarkupRule.new(5.0),
      MaterialMarkupRule.new(['food'], 13.0),
      MaterialMarkupRule.new(['drugs'], 7.5),
      MaterialMarkupRule.new(['electronics'], 2.0),
      PersonsMarkupRule.new(1.2),
      FinalPriceRule.new
  )
  @facts = {}
end

When(/^I calculate it's final price$/) do
  @engine.process(@facts)
end

Given(/^(?:product|it) contains (.*)$/) do |mat|
  unless mat.downcase == 'no special materials'
    @facts[:contains_material] ||= []
    @facts[:contains_material] += mat.downcase.split(/,\s*/)
  end
end

Given(/^(?:product's|it's) base price is \$([\d.]+)$/) do |price|
  @facts[:base_price] = price
end

Given(/^(?:product|it) requires (\d+) persons?$/) do |cnt|
  @facts[:persons_needed] = cnt
end

Then(/^Flat markup of (\d+)% is applied$/) do |flat|
  expect(@facts[:flat_markup] || 0)
      .to eq(flat)
end

Then(/^Additional markup of ([\d.]+)% is applied$/) do |add|
  expect(@facts[:additional_markup] || 0)
      .to eq(add)
end

Then(/^Additional markup is not applied$/) do
  step 'Additional markup of 0% is applied'
end

Then(/^The final price is \$([\d.]+)$/) do |final|
  expect(@facts[:final_price] || 0)
      .to eq(final)
end